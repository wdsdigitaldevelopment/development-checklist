<!DOCTYPE html>
<html lang="en">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<!-- Bootstrap button accordion -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Bootstrap button accordion END -->
		
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700"
    rel="stylesheet" type="text/css">
    <link href="iconFont/cicon/css/cicon-style.css" rel="stylesheet" type="text/css">
    <link href="iconFont/font-awesome/css/cicon-fa-style.css" rel="stylesheet" type="text/css">
    <link href="css/coc-basic-style.css" rel="stylesheet" type="text/css">
    <link href="css/coc-alert-banner.css" rel="stylesheet" type="text/css">
    <!-- Modal window -->
    <link href="css/coc-modal-window.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/jquery.modal.js"></script>
    <!-- Modal window END -->
   <link href="css/styles.css" rel="stylesheet" type="text/css">
    <title>Development checklist</title>
  </head>

  <body id="dc" class="coc-pl">
      <div class="container-fluid">
        <div class="row">
          <div class="side-menu">
          	<nav class="bs-docs-sidenav hidden-print affix side-nav">
							<ul class="nav nav-list">
								<li><a href="#browsers" id="nav-browsers" class="age-scroll active"><span class="cicon-desktop" aria-hidden="true"></span>Browsers</a></li>
								<li><a href="#mobile" id="nav-mobile" class="age-scroll"><span class="cicon-mobile" aria-hidden="true"></span>Mobile</a></li>
								<li><a href="#contents" id="nav-contents" class="age-scroll"><span class="cicon-file-text-o" aria-hidden="true"></span>Contents</a></li>
								<li><a href="#analytics" id="nav-analytics" class="age-scroll"><span class="cicon-line-chart" aria-hidden="true"></span>Analytics</a></li>
								<li><a href="#performance" id="nav-performance" class="age-scroll"><span class="icon-performance" aria-hidden="true"></span>Performance</a></li>
								<li><a href="#usability" id="nav-usability" class="age-scroll"><span class="cicon-thumbs-o-up" aria-hidden="true"></span>Usability</a></li>
								<li><a href="#semantics" id="nav-semantics" class="age-scroll"><span class="cicon-sitemap" aria-hidden="true"></span>Semantics</a></li>
								<li><a href="#code-quality" id="nav-code-quality" class="age-scroll"><span class="cicon-code" aria-hidden="true"></span>Code quality</a></li>
								<li><a href="#accessibility" id="nav-accessibility" class="age-scroll"><span class="cicon-eye-slash" aria-hidden="true"></span>Accessibility</a></li>
								<li><a href="#user-interface" id="nav-user-interface" class="age-scroll"><span class="cicon-check-square-o" aria-hidden="true"></span>User interface</a></li>
								<li><a href="#security" id="nav-security" class="age-scroll"><span class="cicon-lock" aria-hidden="true"></span>Security</a></li>
								<li><a href="#share-point" id="nav-share-point" class="age-scroll"><span class="icon-sharepoint" aria-hidden="true"></span>Share Point</a></li>
							</ul>
						</nav>
					</div>
         
          <div class="main-content">
						<h1>Development checklist</h1>
						<!--<button id="open-all" class="collapsed cui btn-sm secondary-ghost" type="button" data-toggle="collapse" data-target=".resource-list"><span class="cicon-plus-square" aria-hidden="true"></span>Open all link</button>
						<button id="close-all" class="collapsed cui btn-sm secondary-ghost" type="button" data-add="collapse" data-target=".resource-list"><span class="cicon-plus-square" aria-hidden="true"></span>Close all link</button>-->
						
						<!--<button id="toggle-open-close-list" class="collapsed cui btn-sm secondary-ghost" type="button" data-toggle="collapse" data-target=".resource-list"><span id="open-all"><span class="cicon-plus-square" aria-hidden="true"></span>Open all link</span><span id="close-all"><span class="cicon-minus-square" aria-hidden="true"></span>Close all link</span></button>-->
<!-- Browsers -->
						<div id="browsers">
							<h2>Browsers</h2>
							<form action="/action_page.php">
								<div>
									<input type="checkbox" name="browsers" id="check-br-cross-browser" value="Cross browser setting">
									<label for="check-br-cross-browser">Cross browser setting</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#br-cross-browser"></button>
									<ul id="br-cross-browser" class="resource-list collapse">
										<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/bestpractices/Pages/BrowserSupport.aspx" target="_blank">Browser support<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="http://browsershots.org/" target="_blank">BrowserShot<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="https://turbo.net/browsers" target="_blank">Turbo.net<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="https://www.w3schools.com/browsers/default.asp" target="_blank">Browser statistics<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="browsers" id="check-br-responsive" value="Responsiveness">
									<label for="check-br-responsive">Responsiveness</label>
									<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#br-responsive"></button>
									<ul id="br-responsive" class="resource-list collapse">
										<li><a href="#"></a></li>
									</ul>-->
								</div>
							</form>
						</div>
						
<!-- Mobile -->
						<div id="mobile">         
							<h2>Mobile</h2>
							<form action="/action_page.php">
								<div>
									<input type="checkbox" name="mobile" id="check-mb-cross-device" value="Cross device setting">
									<label for="check-mb-cross-device">Cross device setting</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#mb-cross-device"></button>
									<ul id="mb-cross-device" class="resource-list collapse">
										<li><a href="#mobile-device-support" rel="modal:open">Mobile device support &#45; coming soon</a>
											<!-- Modal -->
											<?php include ("includes/modal-mobile-device-support.php");?>
										</li>
										<li><a href="https://search.google.com/search-console/mobile-friendly?utm_source=mft&utm_medium=redirect&utm_campaign=mft-redirect" target="_blank">Mobile-Friendly Test<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="http://gs.statcounter.com/vendor-market-share/mobile/canada" target="_blank">Stat counter &#45; Global Stats &#45; Canada<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>
								

								<div>
									<input type="checkbox" name="mobile" id="check-mb-viewport" value="viewport meta-tag is used">
									<label for="check-mb-viewport">'viewport' meta&#45;tag is used</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#mb-viewport"></button>
									<ul id="mb-viewport" class="resource-list collapse">
										<li><a href="https://www.w3schools.com/css/css_rwd_viewport.asp" target="_blank">Responsive &#45; viewport<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="mobile" id="check-mb-input-type" value="Input types are correct">
									<label for="check-mb-input-type">Input types are correct</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#mb-input-type"></button>
									<ul id="mb-input-type" class="resource-list collapse">
										<li><a href="http://html5tutorial.info/html5-contact.php" target="_blank">Input type: Email, URL, Phone<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>
							</form>
						</div>


<!-- Contents -->
						<div id="contents">          
							<h2>Contents</h2>
							<form action="/action_page.php">
								<div>
									<input type="checkbox" name="content" id="check-con-links" value="All links work">
									<label for="check-con-links">All links work</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-links"></button>
									<ul id="con-links" class="resource-list collapse">
										<li><a href="https://chrome.google.com/webstore/detail/broken-link-checker/nibppfobembgfmejpjaaeocbogeonhch?hl=en" target="_blank">Chrome ad-ons &#45; broken link checker<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="https://validator.w3.org/checklink" target="_blank">W3C link checker<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-spell-grammar" value="Spelling and grammar">
									<label for="check-con-spell-grammar">Spelling and grammar</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-spell-grammar"></button>
									<ul id="con-spell-grammar" class="resource-list collapse">
										<li><a href="https://www.webspellchecker.net/samples/wsc-ckeditor-plugin.html" target="_blank">Web spell checker<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="https://www.w3.org/2002/01/spellchecker" target="_blank">W3C spell checker<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-plain-lang" value="Plain language">
									<label for="check-con-plain-lang">Plain language</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-plain-lang"></button>
									<ul id="con-plain-lang" class="resource-list collapse">
										<li><a href="https://www.webpagefx.com/tools/read-able/" target="_blank">Readability test tool<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-writing-style" value="Writing style follows Canadian press standard">
									<label for="check-con-writing-style">Writing style follows Canadian press standard</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-writing-style"></button>
									<ul id="con-writing-style" class="resource-list collapse">
										<li><a href="#writing-style-guide" rel="modal:open">Writing style guide</a>
										<!-- Modal -->
										<?php include("includes/modal-con-writing-style-guide.php");?>
										</li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-consistent-writing" value="Consistency of writing style">
									<label for="check-con-consistent-writing">Consistency of writing style</label>
									<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-consistent-writing"></button>
									<ul id="con-consistent-writing" class="resource-list collapse">
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>-->
								</div>						

								<div>
									<input type="checkbox" name="content" id="check-con-length-paragraph" value="Length of paragraph &#40;45-75 characters in one line&#41;">
									<label for="check-con-length-paragraph">Length of paragraph &#40;45-75 characters in one line&#41;</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-length-paragraph"></button>
									<ul id="con-length-paragraph" class="resource-list collapse">
										<li><a href="#length-paragraph-test" rel="modal:open">45&#45;75 characters test tool</a>
											<!-- Modal -->
											<?php include("includes/modal-con-length-paragraph.php");?>
										</li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-image-video" value="Images, audio and video work across all supported devices">
									<label for="check-con-image-video">Images, audio and video work across all supported devices</label>
									<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-image-video"></button>
									<ul id="con-image-video" class="resource-list collapse">
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>-->
								</div>

								<div class="indent1">
									<input type="checkbox" name="content" id="check-con-best-practice" value="Best practices are incorporated">
									<label for="check-con-best-practice">Best practices are incorporated</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-best-practice"></button>
									<ul id="con-best-practice" class="resource-list collapse">
										<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/Image/Pages/Image-best-practices.aspx" target="_blank">Image best practices<span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/Video/Pages/Videobestpractices.aspx" target="_blank">Video best practices<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-font-type" value="Corporate font type is used - Open sans for web">
									<label for="check-con-font-type">Corporate font type is used &#45; Open sans for web</label>
									<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-font-type"></button>
									<ul id="con-font-type" class="resource-list collapse">
										<li><a href="http://patternlibrary.calgary.ca/visual-identity/typography.php#typefaces" target="_blank">Typography<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>
								</div>

								<div>
									<input type="checkbox" name="content" id="check-con-font-size" value="Minimum font size is 16px for main contents">
									<label for="check-con-font-size">Minimum font size is 16px for main contents</label>
									<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#con-font-size"></button>
									<ul id="con-font-size" class="resource-list collapse">
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
										<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
									</ul>-->
								</div>
							</form>
					</div>
					

<!-- Analytics -->
					<div id="analytics">         
						<h2>Analytics</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="analytics" id="check-anlys-webtrends" value="Add Webtrends - City's standard analysis tool">
								<label for="check-anlys-webtrends">Add Webtrends &#45; City's standard analysis tool</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#anlys-webtrends"></button>
								<ul id="anlys-webtrends" class="resource-list collapse">
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/measurement/Pages/Webanalyticsmetricsdefinitions.aspx#webtrends" target="_blank">Webtrends definitions<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/measurement/Pages/Measurementbestpractices.aspx" target="_blank">Measurement best practices<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="analytics" id="check-anlys-google-anlys" value="Google analytics">
								<label for="check-anlys-google-anlys">Google analytics &#40;optional&#41;</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#anlys-google-anlys"></button>
								<ul id="anlys-google-anlys" class="resource-list collapse">
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/measurement/Pages/Webanalyticsmetricsdefinitions.aspx#gadefinitions" target="_blank">Google Analytics<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>
					</div>

<!-- Performance -->
					<div id="performance">         
						<h2>Performance</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="performance" id="check-pm-google-speed" value="Google page speed score of 90+">
								<label for="check-pm-google-speed">Google page speed score of 90+</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#pm-google-speed"></button>
								<ul id="pm-google-speed" class="resource-list collapse">
									<li><a href="https://developers.google.com/speed/pagespeed/" target="_blank">google page speed<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="performance" id="check-pm-yslow" value="Yahoo YSlow score of 85+">
								<label for="check-pm-yslow">Yahoo YSlow score of 85+</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#pm-yslow"></button>
								<ul id="pm-yslow" class="resource-list collapse">
									<li><a href="http://yslow.org/" target="_blank">Yahoo &#45; YSlow<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="performance" id="check-pm-http-header" value="Optimize HTTP headers">
								<label for="check-pm-http-header">Optimize HTTP headers</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#pm-http-header"></button>
								<ul id="pm-http-header" class="resource-list collapse">
									<li><a href="https://redbot.org/" target="_blank">redbot.org<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="performance" id="check-pm-opt-img" value="Optimize images">
								<label for="check-pm-opt-img">Optimize images</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#pm-opt-img"></button>
								<ul id="pm-opt-img" class="resource-list collapse">
									<li><a href="https://imageoptim.com/mac" target="_blank">mage optim &#45; for mac<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="https://github.com/google/guetzli" target="_blank">Google Guetzli &#45; github for PC<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="performance" id="check-pm-file-minify" value="Files &#40;JavaScript, HTML, CSS and SVG&#41; are minified">
								<label for="check-pm-file-minify">Files &#40;JavaScript, HTML, CSS and SVG&#41; are minified</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#pm-file-minify"></button>
								<ul id="pm-file-minify" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>
						</form>
					</div>

<!-- Usability -->
					<div id="usability">        
						<h2>Usability</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="usability" id="check-ub-head-tag" value="Titles and meta data in head tag">
								<label for="check-ub-head-tag">Titles and meta data in head tag</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-head-tag"></button>
								<ul id="ub-head-tag" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="usability" id="check-ub-404page" value="Custom 404 page">
								<label for="check-ub-404page">Custom 404 page</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-404page"></button>
								<ul id="ub-404page" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="usability" id="check-ub-favicon" value="Favicon">
								<label for="check-ub-favicon">Favicon</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-favicon"></button>
								<ul id="ub-favicon" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="usability" id="check-ub-urls" value="Friendly URLs">
								<label for="check-ub-urls">Friendly URLs</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-urls"></button>
								<ul id="ub-urls" class="resource-list collapse">
									<li><a href="#" target="_blank"></a></li>
									<li><a href="#" target="_blank"></a></li>
									<li><a href="#" target="_blank"></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="usability" id="check-ub-print-css" value="Print-friendly CSS">
								<label for="check-ub-print-css">Print&#45;friendly CSS</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-print-css"></button>
								<ul id="ub-print-css" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="usability" id="check-ub-js-off" value="The site work with JavaScript turned off">
								<label for="check-ub-js-off">Instruct users to enable Javascript</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-js-off"></button>
								<ul id="ub-js-off" class="resource-list collapse">
									<li><a href="http://www.enable-javascript.com/" target="_blank">How to enable JavaScript in your browser<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="usability" id="check-ub-search-work" value="Search function works">
								<label for="check-ub-search-work">Search function works</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ub-search-work"></button>
								<ul id="ub-search-work" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>
						</form>        		
					</div>	
         		
<!-- Semantics -->
					<div id="semantics">         
						<h2>Semantics</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="semantics" id="check-sm-structure-data" value="structured data">
								<label for="check-sm-structure-data">Add meaning with structured data</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#sm-structure-data"></button>
								<ul id="sm-structure-data" class="resource-list collapse">
									<li><a href="https://www.w3.org/standards/semanticweb/" target="_blank">W3C semantic web<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="semantics" id="check-sm-check-semantics" value="check semantics">
								<label for="check-sm-check-semantics">Check the semantics</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#sm-check-semantics"></button>
								<ul id="sm-check-semantics" class="resource-list collapse">
									<li><a href="https://search.google.com/structured-data/testing-tool" target="_blank">google structured data testing tool<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>       
					</div> 
        
<!-- Code quality -->
					<div id="code-quality">         
						<h2>Code quality</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="code quality" id="check-cq-html-val" value="HTML validation">
								<label for="check-cq-html-val">HTML validation</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#cq-html-val"></button>
								<ul id="cq-html-val" class="resource-list collapse">
									<li><a href="https://validator.w3.org/" target="_blank">W3C HTML validator<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://www.ascii.cl/htmlcodes.htm" target="_blank">HTML code table &#45; Characters and symbols<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="code quality" id="check-cq-css-val" value="CSS validation">
								<label for="check-cq-css-val">CSS validation</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#cq-css-val"></button>
								<ul id="cq-css-val" class="resource-list collapse">
									<li><a href="http://jigsaw.w3.org/css-validator/" target="_blank">W3C CSS validator<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="code quality" id="check-cq-intternational-val" value="International web">
								<label for="check-cq-intternational-val">International web</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#cq-intternational-val"></button>
								<ul id="cq-intternational-val" class="resource-list collapse">
									<li><a href="https://validator.w3.org/i18n-checker/" target="_blank">W3C Internationalization checker<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>       
					</div>
                    
<!-- Accessibility -->
					<div id="accessibility">       
						<h2>Accessibility</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="accessibility" id="check-ac-guideline" value="Accessibility guideline">
								<label for="check-ac-guideline">Accessibility guideline</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-guideline"></button>
								<ul id="ac-guideline" class="resource-list collapse">
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommun" target="_blank">Accessibility standard guideline<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/accessibility/Documents/WCAG-20-AA-evaluation.pdf" target="_blank">Accessibility check list &#45; PDF<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-validation" value="Accessibility validation">
								<label for="check-ac-validation">Accessibility validation</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-validation"></button>
								<ul id="ac-validation" class="resource-list collapse">
									<li><a href="https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh" target="_blank">WAVE Evaluation tool &#45; Chrome add&#45;on<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="https://achecker.ca/checker/index.php" target="_blank">IDI Web accessibility checker<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="https://www.nvaccess.org/" target="_blank">NVDA &#45; Free screen reader &#40;PC users&#41;<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="#" target="_blank">VoiceOver setting &#45; Mac accessibility tool<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://ffoodd.github.io/a11y.css/" target="_blank">code check bookmarklet by CSS<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="#" target="_blank">code check by CSS<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-heading" value="Proper headings">
								<label for="check-ac-heading">Use proper headings</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-heading"></button>
								<ul id="ac-heading" class="resource-list collapse">
									<li><a href="#" target="_blank">How screen user find information<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-h1" value="h1">
								<label for="check-ac-h1">h1 &#45; page or article title &#40;Not website title&#41;</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-h1"></button>
								<ul id="ac-h1" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-skip-nav" value="Skip navigation link">
								<label for="check-ac-skip-nav">Skip navigation link</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-skip-nav"></button>
								<ul id="ac-skip-nav" class="resource-list collapse">
									<li><a href="http://webaim.org/techniques/skipnav/" target="_blank">Skip navigation link &#45; Web AIM<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-colour" value="colour contrast">
								<label for="check-ac-colour">colour contrast</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-colour"></button>
								<ul id="ac-colour" class="resource-list collapse">
									<li><a href="http://www.checkmycolours.com/" target="_blank">Check all colour contrast &#45; online<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="https://www.paciellogroup.com/resources/contrastanalyser/" target="_blank">Downloard colour contrast analyser<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://webaim.org/resources/contrastchecker/" target="_blank">Colour contrast checker &#45; online<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://www.color-blindness.com/coblis-color-blindness-simulator/" target="_blank">Colour blindness checker &#45; online<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-keyboard-control" value="keyboard control">
								<label for="check-ac-keyboard-control">Keyboard control with visible focus</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-keyboard-control"></button>
								<ul id="ac-keyboard-control" class="resource-list collapse">
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/accessibility/Pages/212No-keyboard-trap.aspx" target="_blank">No keyboard trap<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://webaim.org/techniques/keyboard/" target="_blank">Keyboard accessibility &#45; WebAIM<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="https://www.w3.org/TR/UNDERSTANDING-WCAG20/navigation-mechanisms-focus-visible.html" target="_blank">Focus visible &#45; W3C<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/accessibility/Pages/321On-focus.aspx" target="_blank">on Focus rules<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-aria-landmark" value="AIRA landmarks">
								<label for="check-ac-aria-landmark">WAI&#45;ARIA Landmarks</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-aria-landmark"></button>
								<ul id="ac-aria-landmark" class="resource-list collapse">
									<li><a href="https://accessibility.oit.ncsu.edu/using-aria-landmarks-a-demonstration/" target="_blank">WAI&#45;ARIA Landmarks<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-screenreader" value="screenreader support">
								<label for="check-ac-screenreader">Support screenreader using ARIA</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-screenreader"></button>
								<ul id="ac-screenreader" class="resource-list collapse">
									<li><a href="#" target="_blank">ARIA samples<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-form" value="Accessible form">
								<label for="check-ac-form">Accessible form</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-form"></button>
								<ul id="ac-form" class="resource-list collapse">
									<li><a href="http://wet-boew.github.io/v4.0-ci/demos/formvalid/formvalid-en.html" target="_blank">Accessible form template &#45; Government of Canada<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-video-caption" value="Video captions">
								<label for="check-ac-video-caption">Video captions</label>
								<!--<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-video-caption"></button>
								<ul id="ac-video-caption" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>-->
							</div>

							<div>
								<input type="checkbox" name="accessibility" id="check-ac-pdf" value="Optimized PDF">
								<label for="check-ac-pdf">PDF is optimized for web and accessible</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ac-pdf"></button>
								<ul id="ac-pdf" class="resource-list collapse">
									<li><a href="http://webaim.org/techniques/acrobat/acrobat" target="_blank">PDF accessibility &#45; WebAIM<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form> 
					</div>      
                                                                                  
<!-- User interface -->
					<div id="user-interface">
						<h2>User interface</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="user interface" id="check-ui-logo-size" value="logo header size">
								<label for="check-ui-logo-size">Logo and header size are accurate</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ui-logo-size"></button>
								<ul id="ui-logo-size" class="resource-list collapse">
									<li><a href="http://patternlibrary.calgary.ca/components/header.php" target="_blank">Check pattern library &#45; header<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div id="indent1">
								<input type="checkbox" name="user interface" id="check-ui-logo" value="Current logo">
								<label for="check-ui-logo">Current logo is used &#40;Desktop and mobile&#41;</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ui-logo"></button>
								<ul id="ui-logo" class="resource-list collapse">
									<li><a href="http://patternlibrary.calgary.ca/visual-identity/logo.php#primary-identifier" target="_blank">Check pattern library &#45; brand guideline<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://patternlibrary.calgary.ca/visual-identity/visual-identifier-usage.php" target="_blank">Check pattern library &#45; Logo usage<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="user interface" id="check-ui-colour" value="corporate colour">
								<label for="check-ui-colour">Colour &#45; Corporate standard</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ui-colour"></button>
								<ul id="ui-colour" class="resource-list collapse">
									<li><a href="http://patternlibrary.calgary.ca/visual-identity/colours.php#" target="_blank">Check pattern library &#45; colour palette<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="user interface" id="check-ui-buttons" value="buttons">
								<label for="check-ui-buttons">Buttons &#45; Corporate standard</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ui-buttons"></button>
								<ul id="ui-buttons" class="resource-list collapse">
									<li><a href="http://patternlibrary.calgary.ca/elements/buttons.php" target="_blank">Pattern Library &#45; Buttons<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="user interface" id="check-ui-icons" value="icons">
								<label for="check-ui-icons">Icons &#45; Corporate standard</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#ui-icons"></button>
								<ul id="ui-icons" class="resource-list collapse">
									<li><a href="http://patternlibrary.calgary.ca/icons/coc-icons.php" target="_blank">Icon Library<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://patternlibrary.calgary.ca/icons/icons-introduction.php" target="_blank">Pattern Library &#45; icons<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://patternlibrary.calgary.ca/icons/icons-introduction.php#accessibility" target="_blank">Accessibility &#45; icons<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>       
					</div>

<!-- Security -->
					<div id="security">        
						<h2>Security</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="security" id="check-sc-analyzer" value="security analyzer">
								<label for="check-sc-analyzer">ASafaaWeb security analyzer</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#sc-analyzer"></button>
								<ul id="sc-analyzer" class="resource-list collapse">
									<li><a href="https://asafaweb.com/" target="_blank">ASafaaWeb security analyzer<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>       
					</div>	

<!-- SharePoint -->
					<div id="share-point">         
						<h2>SharePoint</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="sharepoint" id="check-sp-image" value="city image standard">
								<label for="check-sp-image">Images are input following City standard</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#sp-image"></button>
								<ul id="sp-image" class="resource-list collapse">
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/Image/Pages/AddingdeletingimagesinSharePoint.aspx" target="_blank">Adding images in SharePoint<span class="cicon-external-link" aria-hidden="true"></span></a></li>
									<li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/ResourceCentre/Image/Pages/AddingaphotogalleryinSharePoint.aspx" target="_blank">How to add a photo galleryry in SharePoint<span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>        
					</div>	

<!-- Title -->          
					<!--
					<div id="id">
						<h2>Title</h2>
						<form action="/action_page.php">
							<div>
								<input type="checkbox" name="Title" id="check-linkName" value="InputName">
								<label for="check-linkName">InputName</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#linkName"></button>
								<ul id="linkName" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="Title" id="check-linkName" value="InputName">
								<label for="check-linkName">InputName</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#linkName"></button>
								<ul id="linkName" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>

							<div>
								<input type="checkbox" name="Title" id="check-linkName" value="InputName">
								<label for="check-linkName">InputName</label>
								<button class="cicon- collapsed" type="button" data-toggle="collapse" data-target="#linkName"></button>
								<ul id="linkName" class="resource-list collapse">
									<li><a href="#" target="_blank"><span class="cicon-external-link" aria-hidden="true"></span></a></li>
								</ul>
							</div>
						</form>
					</div>     -->
         
         	<div class="space"></div>
         	<!-- Footer -->
         	<?php include("includes/footer.php"); ?>                                          
				</div>
<!--End: main-content-->
			</div>
		</div>
    
    <!--<script>
			//toggle open and close all button
				$('#toggle-open-close-list').click(function () {
					$(this).toggleClass('collapsed');
				});
		</script>-->
   	<script>
			//Open & close links
		 	$('.closeall').click(function(){
			$('.resource-list.in')
				.collapse('hide');
				});
				$('.openall').click(function(){
					$('.resource-list:not(".in")')
						.collapse('show');
				});
		</script>
    <script>
			$(document).ready(function () {
			$(document).on("scroll", onScroll);

			//smoothscroll
			$('.nav a[href^="#"]').on('click', function (e) {
					e.preventDefault();
					$(document).off("scroll");

					$('a').each(function () {
							$(this).removeClass('active');
					})
					$(this).addClass('active');

					var target = this.hash,
							menu = target;
					$target = $(target);
					$('html, body').stop().animate({
							'scrollTop': $target.offset().top+2
					}, 500, 'swing', function () {
							window.location.hash = target;
							$(document).on("scroll", onScroll);
							});
					});
			});

			function onScroll(event){
					var scrollPos = $(document).scrollTop();
					$('.nav a').each(function () {
							var currLink = $(this);
							var refElement = $(currLink.attr("href"));
							if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
									$('.nav ul li a').removeClass("active");
									currLink.addClass("active");
							}
							else{
									currLink.removeClass("active");
							}
					});
			}
		</script>

  </body>
</html>