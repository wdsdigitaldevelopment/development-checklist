<div class="cui modalWindow blocker fullheight" id="writing-style-guide" style="display:none;">
	<header class="modal-close-area">
		<a class="cui btn-md primary-text" href="#" rel="modal:close"><span class="sub-nav">Close</span><span class="cicon-times"></span></a>
	 </header>

	 <div class="cui modalbody modalWindow fullheight">
		 <article>
			 <h1>Writing style guide</h1>
			 <p>City of Calgary follows Canadian press style guide for writing</p>
			 <h2>Reference link</h2>
			 <div class="content-info">
				 <ul>
					 <li><a href="docs/style_guide.pdf" target="_blank">Creative Services style guide tip sheet<span class="cicon-external-link" aria-hidden="true"></span></a></li>
					 <li><a href="http://www.thecanadianpress.com/books.aspx?id=182" target="_blank">Canadian press stylebook<span class="cicon-external-link" aria-hidden="true"></span></a></li>
				 </ul>
			 </div>

			 <h2>General tips</h2>
			 <h3>Snake case for general words if more than two words or text length is too long</h3>
			 <div class="content-info">
				 <p>Only first letter of first word is capital letter and the rest of words are lowercase<button class="collapsed accordion2" type="button" data-toggle="collapse" data-target="#writing-snake-case">Why?</button></p>
				 <div id="writing-snake-case" class="resource-list collapse accordion2">
					 <button class="cui btn-md primary-text collapsed close-btn" type="button" data-toggle="collapse" data-target="#writing-snake-case"><span class="sub-nav">Close</span><span class="cicon-times"></span></button>
					 <h3>All capital letters in context is hard to read</h3>
					 <ul>
						 <li>Text in all caps reduces the shape contrast for each word</li>
						 <li>People read uppercase letters more slowly because they don't see them as often and especially people who speak English as second language are affected more</li>
						 <li>These days, text in all caps is perceived as "shouting" so use it sparingly when you need to get someone's attention</li>
					 </ul>
					 <ul>
						<li><a href="http://www.graphics.com/article-old/how-people-read" target="_blank">How people read<span class="cicon-external-link" aria-hidden="true"></span></a></li>
						<li><a href="http://uxmovement.com/content/all-caps-hard-for-users-to-read/" target="_blank">Why text in all uppercase is hard to rea<span class="cicon-external-link" aria-hidden="true"></span></a></li>
					 </ul>
				 </div>

				<strong>Good example</strong>
				 <ul class="indent1">
					 <li>Check this application</li>
					 <li>Application information</li>
					 <li>Comments for city staff</li>
				 </ul>
				 <strong>Bad example</strong>
				 <ul class="indent1">
					 <li>CHECK THIS APPLICATION <span class="inline-text-caution"><span class="cicon-exclamation-triangle" aria-hidden="true"></span>Do not use all uppercase</span></li>
					 <li>Check This Application <span class="inline-text-caution"><span class="cicon-exclamation-triangle" aria-hidden="true"></span>Do not use camel case</span></li>
				 </ul>
			 </div>

			 <h3>Camel case will be used for proper noun</h3>
			 <div class="content-info">
				 <p>Specific name such as department, business unit, application, building, community, and area names</p>
				 <strong>Example</strong>
				 <ul class="indent1">
					 <li>Chief Financial Office</li>
					 <li>Customer Service and Communication</li>
					 <li>Planning and Development Map</li>
					 <li>Highland Park</li>
				 </ul>
			 </div>

			 <h3>Addresses</h3>
			 <div class="content-info">
				 <h4 class="boldFont">Full name</h4>
				 <ul class="indent1">
					 <li>271 Street 43 Avenue North West</li>
					 <li>123 Macleod Trail South East</li>
				 </ul>

				 <h4 class="boldFont">Abbreviation</h4>
				 <ul class="indent1">
					 <li>271 St. 43 Ave. N.W.</li>
					 <li>123 Macleod Trl., S.E.</li>
				 </ul>
				 <a href="https://www.canadapost.ca/tools/pg/manual/PGaddress-e.asp#1437339" target="_blank">
Abbreviations by Canada Post <span class="cicon-external-link" aria-hidden="true"></span></a>

				 <h4 class="boldFont">Important tips</h4>
				 <p>Use full name if only write street or avenue name</p>

				 <div class="content-info">
					 <strong>Good example</strong>
					 <ul class="indent1">
						 <li>5 Avenue</li>
						 <li>Fifth Avenue</li>
					 </ul>
					 <strong>Bad example</strong>
					 <ul class="indent1">
						 <li>5th Avenue <span class="inline-text-caution"><span class="cicon-exclamation-triangle" aria-hidden="true"></span>Do not add "st, nd, and th" after number</span></li>
						 <li>5 Ave. <span class="inline-text-caution"><span class="cicon-exclamation-triangle" aria-hidden="true"></span>Do not use abbreviation</span></li>
					 </ul>
					 <div class="cui alertbox global-inline-alertbox inline-alertbox information" role="alertdialog" aria-labelledby="dialog1Title">
						<div class="alertbanner-box">
							<div class="alert-icon">    
								<span class="cicon cicon-exclamation-circle" aria-hidden="true"></span>
							</div>

							<div class="alertbanner-box-contents">
								<div id="dialog1Title"><span class="boldFont">Information</span> | Abbreviation should be used with numbers</div>
								<ul>
									<li>2-5 Ave.</li>
									<li>123 Macleod Trl.</li>
								</ul>
							</div>   
							<div class="clear"></div>              
						</div>
					 </div>     
				</div>

			 <h3>Choice of words</h3>
			 <div class="content-info">
				<ul>
					<li>Use plain language</li>
					<li>Avoid jargon</li>
					<li>Avoid obsolete, archaic, or invented words</li>
					<li>Avoid slang, regional expressions, and nonstandard English</li>
					<li>Use consistent word
						<div class="display-block">
							<strong>Example</strong>
							<p>If you use "Search", use same word for another place if they are a same thing.
Do not use different words such as "Find out" nor "Check it out".</p>
						</div>
					</li>
				</ul>
			 </div>
			 </div>
		 </article>
		</div>
</div><!-- End: Modal -->