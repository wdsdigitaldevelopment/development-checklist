<footer class="cui coc-footer">
	<div class="footer-group">
		<div class="footerOtherlinks row borderNone">
		<div class="col-md-12 quicklink-wrap">
			<div class="row">
				<div class="col-md-6">

					<div class="row">
					 <div class="col-md-12 col-xs-12">
						 <p class="h3">City's quick links</p>
						 <ul>
							 <li><a href="http://patternlibrary.calgary.ca/index.php">Pattern Library<span class="cicon-external-link" aria-hidden="true"></span></a></li>
							 <li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/Pages/default.aspx" target="_blank">Resource Centre for calgary.ca<span class="cicon-external-link" aria-hidden="true"></span></a></li>
							 <li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/Webstandardsandguidelines/Pages/Default.aspx" target="_blank">Web Standards and Guidelines<span class="cicon-external-link" aria-hidden="true"></span></a></li>
							 <li><a href="http://mycity/OurOrg/Dept/CFO/CustomerServiceCommunications/OurTeam/CMC/webcentral/Pages/default.aspx" target="_blank">Submit a service request<span class="cicon-external-link" aria-hidden="true"></span></a></li>
						 </ul>
					 </div>
					</div>
				 </div>

				 <div class="col-md-6 socialLinks">
					 <p class="h3">Quick links</p>
						 <ul class="col-md-12 col-xs-12">
							 <li><a href="https://www.w3.org/" target="_blank">W3C website<span class="cicon-external-link" aria-hidden="true"></span></a></li>
							 <li><a href="http://webaim.org/" target="_blank">Web AIM &#45; Accessibility<span class="cicon-external-link" aria-hidden="true"></span></a></li>
						 </ul>
				 </div>
			 </div>
		</div>

		<!--<div class="col-md-6">
				 <div class="feedback row">
					<div class="col-lg-7">
						<p>Tell us how to make this page better.</p>
						<p>This should only take a minute.</p>
					</div>
					<div class="col-lg-5">
						<button class="cui btn-md primary">Give feedback</button>
					</div>
				 </div>     
			 </div>-->
		</div>

		<!--<div class="footerlinks">    
			<ul>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Terms of Use</a></li>
				<li><a href="#">Accessibility</a></li>
				<li><a href="#">Site Map</a></li>
				<li><a href="#">SearchTips</a></li>
			</ul>          
		</div>  -->

		<div class="footerCopyright">   
			<p>© 2017. All rights reserved. Official web site of The City of Calgary, located in Calgary, Alberta, Canada.</p>            
		</div>  
	</div>
	<!--end-->
</footer> 