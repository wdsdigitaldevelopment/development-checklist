<div class="cui modalWindow blocker fullheight" id="length-paragraph-test" style="display:none;">
		<header class="modal-close-area">
			<a class="cui btn-md primary-text" href="#" rel="modal:close"><span class="sub-nav">Close</span><span class="cicon-times"></span></a>
	 </header>

	 <div class="cui modalbody modalWindow fullheight">
		 <article>
			 <h1>45&#45;75 characters test tool</h1>
			 <p>Keeping 45&#45;75 characters in one line makes user read contents faster</p>
			 <h2>Add-on to test on your browser</h2>
			 <a href="http://codepen.io/chriscoyier/pen/atebf" target="_blank">Bookmarklet to make the text between 45 and 75 characters turn red<span class="cicon-external-link" aria-hidden="true"></span></a>
		 </article>
	 </div>
</div><!-- End: Modal -->